Role Name
=========

This role pulls a configuration repo and subsequent symlinks to the various
directories and files for a working environment

Requirements
------------

Ubuntu 16+

Role Variables
--------------

TBD2

Dependencies
------------

A configuration repo

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: URL }

License
-------

GPL 1.0

Author Information
------------------

Created by Nathan Black
